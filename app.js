﻿const config = require('config');
const database = require('./config/connexion');
const code = require('./config/secret');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const tediousExpress = require('express4-tedious');
const morgan = require('morgan');
const jwt = require('jsonwebtoken');

const app = express();
app.use(express.static('wwwroot'));

//set secret
app.set('Secret', code.secret);
//set secret
app.set('Connection', database.connection);

// cors 
app.use(cors({credentials: true, origin: 'http://localhost:3000'}));

// use morgan to log requests to the console
app.use(morgan('dev'));

// Setting Base directory
app.use(bodyParser.text({ type: 'application/json' }))
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
// parse application/json
app.use(bodyParser.json());

// const auth = express.Router();
// app.use('/api', auth);

// app.use(function (req, res, next) {
//   //Enabling CORS 
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization");
//   next();
// });

app.use(function (req, res, next) {
    req.sql = tediousExpress(app.get('Connection'));
    next();
});

app.use('/api/urlssites', require('./routes/urlssites'));
app.use('/api/websites', require('./routes/websites'));
app.use('/api/portals', require('./routes/portals'));
app.use('/api/datas', require('./routes/dataclient'));
// app.use('/api/datasjson', require('./routes/datasjson'));

// auth.use((req, res, next) =>{
//   // check header for the token
//   var token = req.headers['access-token'];
//   // decode token
//   if (token) {
//     // verifies secret and checks if the token is expired
//     jwt.verify(token, app.get('Secret'), (err, decoded) =>{
//       if (err) {
//         return res.json({ message: 'invalid token' });
//       } else {
//         // if everything is good, save to request for use in other routes
//         req.decoded = decoded;
//         next();
//       }
//     });
//   } else {
//     // if there is no token  
//     res.send({ 
//         message: 'No token provided.'
//     });
//   }
// });

// app.post('/authenticate', (req,res)=>{
//   if(req.body.username === "aymen"){
//       if(req.body.password == 123){
//            //if eveything is okey let's create our token 
//       const payload = {
//           check:  true
//         };
//         var token = jwt.sign(payload, app.get('Secret'), {
//               expiresIn: 1440 // expires in 24 hours
//         });
//         res.json({
//           message: 'authentication done ',
//           token: token
//         });
//       }else{
//           res.json({message:"please check your password !"})
//       }
//   }else{
//       res.json({message:"user not found !"})
//   }
// })

// auth.use(bodyParser.text({ type: 'application/json' }))

// auth.use(function (req, res, next) {
//   req.sql = tediousExpress(app.get('Connection'));
//   next();
// });

// auth.use('/urlssites', require('./routes/urlssites'));
// auth.use('/websites', require('./routes/websites'));
// auth.use('/portals', require('./routes/portals'));
// auth.use('/datas', require('./routes/dataclient'));
// // auth.use('/api/datasjson', require('./routes/datasjson'));


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error('Not Found' + req.originalUrl);
    err.status = 404;
    next(err);
});

app.set('port', process.env.PORT || 3000);

let server = app.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + server.address().port);
});

module.exports = app;
