
CREATE PROCEDURE dbo.InsertWebSitesJson(@webSitesJson NVARCHAR(MAX))
AS BEGIN

  INSERT INTO dbo.WebSites (siteName,abName,quotaUrls,quotaEv,duration)
  OUTPUT  INSERTED.siteID
  SELECT siteName,abName,quotaUrls,quotaEv,duration
  FROM OPENJSON(@webSitesJson, N'$')
    WITH (
      siteName VARCHAR(255) N'$."siteName"',
      abName VARCHAR(50) N'$."abName"',
      quotaUrls SMALLINT N'$."quotaUrls"',
      quotaEv INT N'$."quotaEv"',
      duration SMALLINT N'$."duration"')
END
GO

-- DECLARE @json NVARCHAR(MAX) 
-- SET @json = N'[{"siteName":"MonSite2ouf","abName":"premium","quotaUrls":4,"quotaEv":40000,"duration":30}]'
-- SELECT @json;
-- EXEC InsertWebSitesJson @json;


CREATE PROCEDURE dbo.UpdateWebSitesJson(@siteID int, @webSitesJson NVARCHAR(MAX))
AS BEGIN

  MERGE INTO dbo.WebSites
  USING ( SELECT siteName,abName,quotaUrls,quotaEv,duration
    FROM OPENJSON(@webSitesJson)
      WITH (
        siteName VARCHAR(255) N'$."siteName"',
        abName VARCHAR(50) N'$."abName"',
        quotaUrls SMALLINT N'$."quotaUrls"',
        quotaEv INT N'$."quotaEv"',
        duration SMALLINT N'$."duration"')) as json
  ON (dbo.WebSites.siteID = @siteID)
  WHEN MATCHED THEN
    UPDATE SET
      siteName = json.siteName,
      abName = json.abName,
      quotaUrls = json.quotaUrls,
      quotaEv = json.quotaEv,
      duration = json.duration
  WHEN NOT MATCHED THEN 
    INSERT (siteName,abName,quotaUrls,quotaEv,duration)
    VALUES (json.siteName,json.abName,json.quotaUrls,json.quotaEv,json.duration);
END
GO

-- DECLARE @siteID INT, @webSitesJson NVARCHAR(MAX);
-- SET @webSitesJson = N'[{"siteName":"MonSite2ouf","abName":"gratuit","quotaUrls":4,"quotaEv":40000,"duration":30}]';
-- SET @siteID = 1;
-- SELECT @siteID, @websitesJson;
-- EXEC UpdateWebSitesJson @siteID, @websitesJson;


CREATE PROCEDURE dbo.InsertUrlsSitesJson(@urlsSitesJson NVARCHAR(MAX))
AS BEGIN

  INSERT INTO dbo.UrlsSites (urlsCreated_at,urls,sumUrls,sumUrlsEv,siteIDFK)
  OUTPUT  INSERTED.urlsID
  SELECT urlsCreated_at,urls,sumUrls,sumUrlsEv,siteIDFK
  FROM OPENJSON(@urlsSitesJson, N'$')
    WITH (
      urlsCreated_at DATETIME N'$."urlsCreated_at"',
      urls NVARCHAR(MAX) N'$."urls"' AS JSON,
      sumUrls SMALLINT N'$."sumUrls"',
      sumUrlsEv SMALLINT N'$."sumUrlsEv"',
      siteIDFK INT N'$."siteIDFK"')
END
GO

DECLARE @json NVARCHAR(MAX) 
SET @json = N'[{"urlsCreated_at":"22-09-2019 11:35:36.127","urls":{"urlID":0,"url":"http:\/\/localhost\/apiJson?article=1","urlCreated_at":"22-09-2019 11:35:36.127","beginDate":"22-09-2019 11:35:36.127","endDate":null,"interval":"02:01","totEv":null,"urlStatus":1},"sumUrls":1,"sumUrlsEv":0,"siteIDFK":5}]'
SELECT @json;
EXEC InsertUrlsSitesJson @json;

CREATE PROCEDURE dbo.GetUrlsSitesBySite(@siteID INT)
AS BEGIN
  DECLARE @json1 nvarchar(max),
          @json2 nvarchar(max)

  DECLARE @result AS nvarchar(max)

  SET @json1 = (SELECT IDENT_CURRENT('dbo.UrlsSites') AS urlsIDmax FOR JSON PATH, WITHOUT_ARRAY_WRAPPER)
  SET @json2 = (SELECT urlsID,urlsCreated_at,urls,sumUrls,sumUrlsEv,siteIDFK FROM UrlsSites WHERE siteIDFK=@siteID FOR JSON PATH, ROOT('data'))

  SELECT
    @result = COALESCE(@result + ', ', '') + '"' + [key] + '":"' + value + '"'
  FROM (SELECT
    [key],
    value
  FROM OPENJSON(@json1)
  UNION ALL
  SELECT
    [key],
    value
  FROM OPENJSON(@json2)) AS x

  SET @result = '{' + @result + '}'

  PRINT @result
END
GO

-- DECLARE @siteID INT = 1;
-- EXEC GetUrlsSitesBySite @siteID;

-- DECLARE @urlID NVARCHAR(MAX) ='{"urlID": 0}'
-- PRINT @urlID
-- -- Increment value  
-- SET @urlID = JSON_MODIFY(@urlID,'$.urlID',
-- CAST(JSON_VALUE(@urlID,'$.urlID') AS INT) + 1)
-- PRINT @urlID

CREATE PROCEDURE dbo.UpdateUrlsSitesJson(@urlsID int, @urlsSitesJson NVARCHAR(MAX))
AS BEGIN

  MERGE INTO dbo.UrlsSites
  USING ( SELECT urlsCreated_at,urls,sumUrls,sumUrlsEv,siteIDFK
    FROM OPENJSON(@urlsSitesJson)
      WITH (
        urlsCreated_at DATETIME N'$."urlsCreated_at"',
        urls NVARCHAR(MAX) N'$."urls"' AS JSON,
        sumUrls SMALLINT N'$."sumUrls"',
        sumUrlsEv SMALLINT N'$."sumUrlsEv"',
        siteIDFK INT N'$."siteIDFK"')) as json
  ON (dbo.UrlsSites.urlsID = @urlsID)
  WHEN MATCHED THEN 
    UPDATE SET
      urlsCreated_at = json.urlsCreated_at,
      urls = json.urls,
      sumUrls = json.sumUrls,
      sumUrlsEv = json.sumUrlsEv,
      siteIDFK = json.siteIDFK
  WHEN NOT MATCHED THEN 
    INSERT (urlsCreated_at,urls,sumUrls,sumUrlsEv,siteIDFK)
    VALUES (json.urlsCreated_at,json.urls,json.sumUrls,json.sumUrlsEv,json.siteIDFK);
END
GO

-- DECLARE @urlsID INT, @urlsSitesJson NVARCHAR(MAX);
-- SET @urlsSitesJson = N'[{"urlsCreated_at":"22-09-2019 11:35:36.127","urls":{"urlID":0,"url":"http:\/\/localhost\/apiJson?article=1","urlCreated_at":"22-09-2019 11:35:36.127","beginDate":"22-09-2019 11:35:36.127","endDate":null,"interval":"02:01","totEv":null,"urlStatus":1},"sumUrls":1,"sumUrlsEv":0}]';
-- SET @urlsID = 1;
-- SELECT @urlsID, @urlsSitesJson;
-- EXEC UpdateUrlsSitesJson @urlsID, @urlsSitesJson;

CREATE PROCEDURE dbo.InsertPortalsSitesJson(@portalsSitesJson NVARCHAR(MAX))
AS BEGIN

  INSERT INTO dbo.PortalsSites (eventCreated_at,urlName,intervalEv,newUrl,beginEv,endEv,statusEv,urlsIDFK,dataJson)
  OUTPUT  INSERTED.eventID
  SELECT eventCreated_at,urlName,intervalEv,newUrl,beginEv,endEv,statusEv,urlsIDFK,dataJson
  FROM OPENJSON(@portalsSitesJson, N'$')
    WITH (
      eventCreated_at DATETIME N'$."eventCreated_at"',
      urlName NVARCHAR(2083) N'$."urlName"',
      intervalEv INT N'$."intervalEv"',
      newUrl NVARCHAR(2083) N'$."newUrl"',
      beginEv DATETIME N'$."beginEv"',
      endEv DATETIME N'$."endEv"',
      statusEv BIT N'$."statusEv"',
      urlsIDFK INT N'$."urlsIDFK"',
      dataJson NVARCHAR(MAX) AS JSON)
END
GO

-- DECLARE @json NVARCHAR(MAX) 
-- SET @json = N'[{"eventCreated_at":"22-09-2019 11:35:36.127","urlName":"http://localhost:3000/api/urlssites","intervalEv":1,"newUrl":"?date=1567930865","beginEv":"22-09-2019 11:35:36.127","endEv":"22-09-2019 11:35:36.127","statusEv":0,"urlsIDFK":413,"dataJson":null}]';
-- SELECT @json;
-- EXEC InsertPortalsSitesJson @json;


CREATE PROCEDURE dbo.UpdatePortalsSitesJson(@eventID int, @portalsSitesJson NVARCHAR(MAX))
AS BEGIN

  MERGE INTO dbo.PortalsSites
  USING ( SELECT eventCreated_at,urlName,intervalEv,newUrl,beginEv,endEv,statusEv,urlsIDFK,dataJson
    FROM OPENJSON(@portalsSitesJson)
      WITH (
      eventCreated_at DATETIME N'$."eventCreated_at"',
      urlName NVARCHAR(2083) N'$."urlName"',
      intervalEv INT N'$."intervalEv"',
      newUrl NVARCHAR(2083) N'$."newUrl"',
      beginEv DATETIME N'$."beginEv"',
      endEv DATETIME N'$."endEv"',
      statusEv BIT N'$."statusEv"',
      urlsIDFK INT N'$."urlsIDFK"',
      dataJson NVARCHAR(MAX))) as json
  ON (dbo.PortalsSites.eventID = @eventID)
  WHEN MATCHED THEN 
    UPDATE SET
      eventCreated_at = json.eventCreated_at,
      urlName = json.urlName,
      intervalEv = json.intervalEv,
      newUrl = json.newUrl,
      beginEv = json.beginEv,
      endEv = json.endEv,
      statusEv = json.statusEv,
      urlsIDFK = json.urlsIDFK,
      dataJson = json.dataJson
  WHEN NOT MATCHED THEN 
    INSERT (eventCreated_at,urlName,intervalEv,newUrl,beginEv,endEv,statusEv,urlsIDFK,dataJson)
    VALUES (json.eventCreated_at,json.urlName,json.intervalEv,json.newUrl,json.beginEv,json.endEv,json.statusEv,json.urlsIDFK,json.dataJson);
END
GO

-- DECLARE @eventID INT, @portalsSitesJson NVARCHAR(MAX);
-- SET @portalsSitesJson = N'[{"eventCreated_at":"22-09-2019 11:35:36.127","urlName":"http:\/\/localhost\/apiJson?article=1","intervalEv":"02:01","newUrl":"?event=289&date=1567930865","beginEv":"22-09-2019 11:35:36.127","endEv":"22-09-2019 11:35:36.127","statusEv":0,"urlID":0,"urlsID":12}]';
-- SET @eventID = 1;
-- SELECT @eventID, @portalsSitesJson;
-- EXEC UpdatePortalsSitesJson @eventID, @portalsSitesJson;


CREATE PROCEDURE dbo.UpdatePortalsSitesWithJson(@eventID int, @portalsSitesJson NVARCHAR(MAX))
AS BEGIN

  DECLARE @response NVARCHAR(MAX), @urlName NVARCHAR(2083), @dataJson NVARCHAR(MAX), @isJson INT;
  SET @urlName = (SELECT urlName FROM OPENJSON(@portalsSitesJson, N'$') WITH (urlName NVARCHAR(2083))); 

  EXECUTE dbo.GetWebService @urlName, @response OUTPUT;

  SET @dataJson = (SELECT @response);
  SET @isJson = (SELECT ISJSON(@dataJson));

  MERGE INTO dbo.PortalsSites
  USING ( SELECT eventCreated_at,urlName,intervalEv,newUrl,beginEv,endEv,statusEv,urlsIDFK,dataJson
    FROM OPENJSON(@portalsSitesJson, N'$')
      WITH (
      eventCreated_at DATETIME N'$."eventCreated_at"',
      urlName NVARCHAR(2083) N'$."urlName"',
      intervalEv INT N'$."intervalEv"',
      newUrl NVARCHAR(2083) N'$."newUrl"',
      beginEv DATETIME N'$."beginEv"',
      endEv DATETIME N'$."endEv"',
      statusEv BIT N'$."statusEv"',
      urlsIDFK INT N'$."urlsIDFK"',
      dataJson NVARCHAR(MAX) N'$.dataJson' AS JSON)) as json
  ON (dbo.PortalsSites.eventID = @eventID)
  WHEN MATCHED THEN 
    UPDATE SET
      eventCreated_at = json.eventCreated_at,
      urlName = json.urlName,
      intervalEv = json.intervalEv,
      newUrl = json.newUrl,
      beginEv = json.beginEv,
      endEv = json.endEv,
      statusEv = json.statusEv,
      urlsIDFK = json.urlsIDFK,
      dataJson = @dataJson
  WHEN NOT MATCHED THEN 
    INSERT (eventCreated_at,urlName,intervalEv,newUrl,beginEv,endEv,statusEv,urlsIDFK,dataJson)
    VALUES (json.eventCreated_at,json.urlName,json.intervalEv,json.newUrl,json.beginEv,json.endEv,json.statusEv,json.urlsIDFK,@dataJson);
END
GO

-- DECLARE @eventID INT, @portalsSitesJson NVARCHAR(MAX);
-- SET @portalsSitesJson = N'[{"eventCreated_at":"22-09-2019 11:35:36.127","urlName":"http:\/\/localhost:3000\/api\/urlssites","intervalEv":1,"newUrl":"?event=289&date=1567930865","beginEv":"22-09-2019 11:35:36.127","endEv":"22-09-2019 11:35:36.127","statusEv":1,"urlsIDFK":411,"dataJson":{"X-Cloud-Trace-Context":"cd8b09abf2e674c4d91e9c6f642fe2cf/16812763448502983439","Accept":"*/*","User-Agent":"Mozilla/4.0(compatible;Win32;WinHttp.WinHttpRequest.5)","Host":"headers.jsontest.com","Accept-Language":"fr-FR,fr;q=0.5","Content-Type":"application/x-www-form-urlencoded"}}]';
-- SET @eventID = 2237;
-- SELECT @eventID, @portalsSitesJson;
-- EXEC UpdatePortalsSitesWithJson @eventID, @portalsSitesJson;


CREATE PROCEDURE dbo.InsertPortalsSitesWithJson(@portalsSitesJson NVARCHAR(MAX))
AS BEGIN
  
  DECLARE @response NVARCHAR(MAX), @urlName NVARCHAR(2083), @dataJson NVARCHAR(MAX), @isJson INT;
  SET @urlName = (SELECT urlName FROM OPENJSON(@portalsSitesJson, N'$') WITH (urlName NVARCHAR(2083))); 

  EXECUTE dbo.GetWebService @urlName, @response OUTPUT;

  SET @dataJson = (SELECT @response);
  SET @isJson = (SELECT ISJSON(@dataJson));

  INSERT INTO dbo.PortalsSites (eventCreated_at,urlName,intervalEv,newUrl,beginEv,endEv,statusEv,urlsIDFK,dataJson)
  OUTPUT  INSERTED.eventID
  SELECT eventCreated_at,urlName,intervalEv,newUrl,beginEv,endEv,statusEv,urlsIDFK,@dataJson
  FROM OPENJSON(@portalsSitesJson, N'$')
    WITH (
      eventCreated_at DATETIME N'$."eventCreated_at"',
      urlName NVARCHAR(2083) N'$."urlName"',
      intervalEv INT N'$."intervalEv"',
      newUrl NVARCHAR(2083) N'$."newUrl"',
      beginEv DATETIME N'$."beginEv"',
      endEv DATETIME N'$."endEv"',
      statusEv BIT N'$."statusEv"',
      urlsIDFK INT N'$."urlsIDFK"',
      dataJson NVARCHAR(MAX) AS JSON)

END
GO