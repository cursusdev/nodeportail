---------------------------------------Requête de test pour UrlsSites
DECLARE @json NVARCHAR(MAX)
SET @json = N'[{"urlsCreated_at":"22-09-2019 11:35:36.127","urls":{"url":"http:\/\/localhost:3000\/api\/urlssites","urlCreated_at":"22-09-2019 11:35:36.127","beginDate":"22-09-2019 11:35:36.127","endDate":null,"interval":120,"totEv":0,"urlStatus":0},"sumUrls":1,"sumUrlsEv":0,"siteIDFK":1}]'

INSERT INTO dbo.UrlsSites (urlsCreated_at,urls,sumUrls,sumUrlsEv,siteIDFK)
OUTPUT  INSERTED.urlsID
SELECT urlsCreated_at,urls,sumUrls,sumUrlsEv,siteIDFK
FROM OPENJSON(@json, N'$')
  WITH (
    urlsCreated_at DATETIME N'$."urlsCreated_at"',
    urls NVARCHAR(MAX) N'$."urls"' AS JSON,
    sumUrls SMALLINT N'$."sumUrls"',
    sumUrlsEv SMALLINT N'$."sumUrlsEv"',
    siteIDFK INT N'$."siteIDFK"')

SELECT urlsID,urlsCreated_at,urls,sumUrls,sumUrlsEv,siteIDFK FROM UrlsSites FOR JSON PATH


