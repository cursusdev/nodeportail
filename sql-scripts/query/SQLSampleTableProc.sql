--Create a table to store the names of Stored Procedures which need to be 
--Executed by using SSIS Package

USE ProductCatalog
GO

  CREATE TABLE dbo.ProcList (
    DatabaseName VARCHAR(100) NOT NULL,
    SchemaName VARCHAR(100) NOT NULL,
    ProcName VARCHAR(100) NOT NULL)

  --Create Sample Tables
  CREATE TABLE ProductCatalog.dbo.Portails (
    id INT,
    siteName VARCHAR(100) NOT NULL
    urlApiSite VARCHAR(255) NOT NULL)

  CREATE TABLE ProductCatalog.dbo.LogJson (
    LogID INT,
    siteName VARCHAR(100) NOT NULL
    urlApiSite varchar(255) NOT NULL
    jsonResp NVARCHAR(max) NULL
    dateLog DATE(NOW) NOT NULL)

  --Create couple of Stored Procedure in Different Databases
  --Create SPs in different Database with different Schemas
  USE ProductCatalog
  GO
 --drop procedure dbo.LoadPortails
  GO
  CREATE PROCEDURE dbo.LoadPortails
  AS
  BEGIN
  INSERT INTO dbo.Portails(procID,siteName,urlApiSite,statusPortail,dateProc)
  SELECT
    NEWID() AS procID,siteName,urlApiSite,statusPortail,CAST([DateTime] AS DATE) as dt
  FROM dbo.sites
  WHERE dateLoad > NOW
  INSERT INTO dbo.Portails
  VALUES (1,'siteA', 'http://localhost:8080/portailphp/siteA/articles','gratuit','08/08/2019')
  VALUES (2,'siteB', 'http://localhost:8080/portailphp/siteB/data','forfait1','08/08/2019')
  VALUES (3,'siteB', 'http://localhost:8080/portailphp/siteC/data.json','backup','08/08/2019')
  END

  GO
  CREATE PROCEDURE dbo.LoadJonWeb
  ...
  INSERT INTO dbo.JsonWeb
  VALUES (1, 'siteA', 'http://localhost:8080/portailphp/siteA/data', NULL, '08/08/2019', 'gratuit')
  VALUES (2, 'siteB', 'http://localhost:8080/portailphp/siteB/data', '{}', '08/08/2019', 'forfait1')
  VALUES (3, 'siteC', 'http://localhost:8080/portailphp/siteC/data.json', '{}', '08/08/2019', 'backup')
  END

  --Insert the names of Stored Procedures with Database and Schema Name in dbo.ProcList Table
  INSERT INTO dbo.ProcList VALUES ('ProductCatalog','dbo','LoadPortails')
  GO
  INSERT INTO dbo.ProcList VALUES ('ProductCatalog','dbo','LogJson')

  --Query to be used in Execute SQL Task 
  SELECT DatabaseName,SchemaName,ProcName FROM dbo.ProcList
  
  
  --After Executing The SSIS Package validate your records if SPs ran successfully
  SELECT * FROM dbo.Portails
  SELECT * FROM dbo.LogJson

 