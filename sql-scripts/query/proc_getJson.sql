 /* url source https://www.red-gate.com/simple-talk/sql/t-sql-programming/importing-json-web-services-applications-sql-server/  */

/* Note: >
    OLE Automation objects can be used within a Transact-SQL batch, but 
    SQL Server blocks access to OLE Automation stored procedures because
    this component is turned off as part of the security configuration. */

USE Portals
GO
---------------------Affichage des options de configuration avancées
sp_configure 'show advanced options', 1;
GO
RECONFIGURE;

GO
sp_configure 'Ole Automation Procedures', 1;
GO
RECONFIGURE;

GO
sp_configure 'external scripts enabled', 1;
GO
RECONFIGURE WITH OVERRIDE; 

/* ou 	EXECUTE sp_configure 'Ole Automation Procedures', 1; */

/* Grant authorization */
USE master
GO
grant exec on sp_OACreate to cursusdev
GO
grant exec on sp_OAMethod to cursusdev
GO

-- GRANT EXECUTE ON [sys].[sp_OACreate] TO [YOURDOMAIN\YourUser]
-- GO
-- GRANT EXECUTE ON [sys].[sp_OADestroy] TO [YOURDOMAIN\YourUser]
-- GO
-- GRANT EXECUTE ON [sys].[sp_OAGetErrorInfo] TO [YOURDOMAIN\YourUser]
-- GO
-- GRANT EXECUTE ON [sys].[sp_OAGetProperty] TO [YOURDOMAIN\YourUser]
-- GO
-- GRANT EXECUTE ON [sys].[sp_OAMethod] TO [YOURDOMAIN\YourUser]
-- GO
-- GRANT EXECUTE ON [sys].[sp_OAStop] TO [YOURDOMAIN\YourUser]
-- GO
-- GRANT EXECUTE ON [sys].[sp_OASetProperty] TO [YOURDOMAIN\YourUser]
-- GO

-- SELECT * 
-- FROM master.sys.database_permissions [dp] 
-- JOIN master.sys.system_objects [so] ON dp.major_id = so.object_id
-- JOIN master.sys.sysusers [usr] ON 
--      usr.uid = dp.grantee_principal_id AND usr.name = 'cursusdev'
-- WHERE permission_name = 'EXECUTE' AND so.name = 'sp_OACreate'


IF NOT EXISTS (SELECT * FROM sys.configurations WHERE name ='Ole Automation Procedures' AND value=1)
  	BEGIN
     EXECUTE sp_configure 'Ole Automation Procedures', 1;
     RECONFIGURE;  
     end 
  SET ANSI_NULLS ON;
  SET QUOTED_IDENTIFIER ON;
  GO
  IF Object_Id('dbo.GetWebService','P') IS NOT NULL 
  	DROP procedure dbo.GetWebService
  GO
  CREATE PROCEDURE dbo.GetWebService
    @TheURL VARCHAR(2083),-- the url of the web service
    @TheResponse NVARCHAR(4000) OUTPUT --the resulting JSON
  AS
    BEGIN
      DECLARE @obj INT, @hr INT, @status INT, @message VARCHAR(255);
 
      EXEC @hr = sp_OACreate 'MSXML2.ServerXMLHttp', @obj OUT;
      SET @message = 'sp_OAMethod Open failed';
      IF @hr = 0 EXEC @hr = sp_OAMethod @obj, 'open', NULL, 'GET', @TheURL, false;
      SET @message = 'sp_OAMethod setRequestHeader failed';
      IF @hr = 0
        EXEC @hr = sp_OAMethod @obj, 'setRequestHeader', NULL, 'Content-Type',
          'application/x-www-form-urlencoded';
      SET @message = 'sp_OAMethod Send failed';
      IF @hr = 0 EXEC @hr = sp_OAMethod @obj, send, NULL, '';
      SET @message = 'sp_OAMethod read status failed';
      IF @hr = 0 EXEC @hr = sp_OAGetProperty @obj, 'status', @status OUT;
      IF @status <> 200 BEGIN
        SELECT @message = 'sp_OAMethod http status ' + Str(@status), @hr = -1;
        END;
      SET @message = 'sp_OAMethod read response failed';
      IF @hr = 0
        BEGIN
          EXEC @hr = sp_OAGetProperty @obj, 'responseText', @Theresponse OUT;
          END;
      EXEC sp_OADestroy @obj;
      IF @hr <> 0 RAISERROR(@message, 16, 1);
      END;
  GO

-- DECLARE @response NVARCHAR(MAX) 
-- EXECUTE dbo.GetWebService 'http://date.jsontest.com/', @response OUTPUT
-- SELECT  @response

    /**
Examples:
    - >
    DECLARE @response NVARCHAR(MAX) 
    EXECUTE dbo.GetWebService 'http://headers.jsontest.com/', @response OUTPUT
    SELECT  @response 
Returns: >
  nothing
**/


/**
Installez le package Jsonlite pour le service SQL Server R
**/
EXEC sp_execute_external_script
    @language = N'R'
    ,@script = N'print("R script enable")'
-- Response: Message(s) STDOUT provenant du script externe : [1] "R script enable"

EXEC sp_execute_external_script
      @language = N'R'
      , @script = N'	library(jsonlite)'
 -- Response: Error in library(jsonlite) : aucun package nommé 'jsonlite' n'est trouvé
 -- ou "commande russie"

-- Run as administrator:
-- C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\R_SERVICES\bin\R.exe
-- install.packages("jsonlite")
-- install.packages("curl")

GRANT EXECUTE ANY EXTERNAL SCRIPT TO cursusdev

------------------------------------------------Https

DECLARE @status int
DECLARE @responseText as table(responseText nvarchar(max))
DECLARE @res as Int;
DECLARE @url as nvarchar(1000) = 'https://financialmodelingprep.com/api/v3/company/profile/AAPL'
EXEC sp_OACreate 'MSXML2.ServerXMLHTTP', @res OUT
EXEC sp_OAMethod @res, 'open', NULL, 'GET',@url,'false'
EXEC sp_OAMethod @res, 'send'
EXEC sp_OAGetProperty @res, 'status', @status OUT
INSERT INTO @ResponseText (ResponseText) EXEC sp_OAGetProperty @res, 'responseText'
EXEC sp_OADestroy @res
SELECT @status, responseText FROM @responseText