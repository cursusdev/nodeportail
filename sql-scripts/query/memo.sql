-- single sample
EXECUTE sp_execute_external_script    
    @language = N'R'    
    ,@script=N'
            library(jsonlite)
            js <- InputDataSet
            js2 <- as.character(toJSON(js))
            OutputDataSet <- data.frame(fromJSON(js2))'
    ,@input_data_1 = N'SELECT top 10 object_id  FROM sys.objects FOR JSON AUTO'
WITH RESULT SETS ((nr INT));


-- Retrieving installed packages
EXECUTE [sys].[sp_execute_external_script] 
 @language = N'R'
 ,@script = N'AllPackages <- as.data.frame(installed.packages())
 OutputDataSet <- AllPackages[c("Package", "LibPath", "Version", "Priority", "Depends", "Imports", "LinkingTo", "Suggests", "Enhances", "OS_type", "License", "Built")]'
WITH RESULT SETS((PackageName NVARCHAR (100), 
  LibPath NVARCHAR (255), 
  Version NVARCHAR (20), 
  Priority NVARCHAR (20), 
  Depends NVARCHAR (255), 
  Imports NVARCHAR (100), 
  LinkingTo NVARCHAR (100), 
  Suggests NVARCHAR (100), 
  Enhances NVARCHAR (100), 
  OS_type NVARCHAR (100), 
  License NVARCHAR (100),
  Built NVARCHAR (100)))


