-----------------DEVELOPPEMENT
USE Portals
-- Crée le login avec mot de passe
CREATE LOGIN cursusdev WITH PASSWORD = 'cursus@2019';
GO
-- Créer un utilisateur de base de données pour la connexion créée ci-dessus
CREATE USER cursusdev FOR LOGIN cursusdev

-- DROP LOGIN cursusdev  --loginName
-- DROP USER cursusdev   -- userName
-----------------PRODUCTION
-- Login purement SQL: compte de connexion permet de se connecter au serveur
DECLARE 
	@loginName VARCHAR(255),
	@SqlStatement NVARCHAR(255)

IF NOT EXISTS (SELECT loginName FROM master.dbo.syslogins 
    WHERE name = @loginName AND dbname = 'Portals')
BEGIN
    SELECT @SqlStatement = 'CREATE LOGIN ' + QUOTENAME(@loginName) + ' FROM WINDOWS WITH DEFAULT_DATABASE=[Portals], DEFAULT_LANGUAGE=[us_english]'

    EXEC sp_executesql @SqlStatement
END

SELECT * FROM sys.syslogins

