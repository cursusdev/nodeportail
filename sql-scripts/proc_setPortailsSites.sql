CREATE PROCEDURE dbo.SetPortalsSites
AS BEGIN

	DECLARE @today DATETIME;
	DECLARE @response NVARCHAR(MAX) 
	SET DATEFORMAT ymd
	SET @today = (SELECT GETDATE());
	--SET @dateEv = (SELECT CAST(CONVERT(CHAR(8), GETDATE(), 112) AS INT));

	DROP TABLE IF EXISTS #moveID
	CREATE TABLE #moveID (
		moveID INT PRIMARY KEY IDENTITY (1, 1),
		eventID INT,
		eventCreated_at DATETIME,
		urlName NVARCHAR(2083),
		intervalEv INT ,
		newUrl NVARCHAR(2083),
	   beginEv DATETIME,
	  endEv DATETIME,
	  statusEv BIT,
	 urlsIDFK INT
	)

	INSERT INTO #moveID (eventID,eventCreated_at,urlName,intervalEv,newUrl,beginEv,endEv,statusEv,urlsIDFK)
	SELECT eventID,eventCreated_at,urlName,intervalEv,newUrl,beginEv,endEv,statusEv,urlsIDFK
	FROM dbo.PortalsSites WHERE statusEv = 0 AND dataJson IS NULL;
	-- Le portail doit être actif et égale à aujourdh'hui

	DECLARE @evID INT,
			@eventID INT,
			@eventCreated_at DATETIME,
			@urlName NVARCHAR(2083),
			@intervalEv INT,
			@newUrl NVARCHAR(2083),
			@beginEv DATETIME,
			@endEv DATETIME,
			@statusEv BIT,
			@urlsIDFK INT,
			@portalsSitesUpdate NVARCHAR(4000),
			@newPortalsSites NVARCHAR(MAX),
			@newBeginEv DATETIME,
			@newEndEv DATETIME,
			@newStatusEv BIT;
	WHILE EXISTS(SELECT * FROM #moveID)
	BEGIN
		SET @evID = (SELECT TOP 1 moveID FROM #moveID);
		SET @eventID = (SELECT eventID FROM #moveID WHERE moveID = @evID);
		SET @eventCreated_at = (SELECT eventCreated_at FROM #moveID WHERE moveID = @evID);
		SET @urlName = (SELECT urlName FROM #moveID WHERE moveID = @evID);
		SET @intervalEv = (SELECT intervalEv FROM #moveID WHERE moveID = @evID);
		SET @newUrl = (SELECT newUrl FROM #moveID WHERE moveID = @evID);
		SET @beginEv = (SELECT beginEv FROM #moveID WHERE moveID = @evID);
		SET @endEv = (SELECT endEv FROM #moveID WHERE moveID = @evID);
		SET @statusEv = 1;
		SET @urlsIDFK = (SELECT urlsIDFK FROM #moveID WHERE moveID = @evID);
		SET @portalsSitesUpdate = N'[{"eventCreated_at":"' + (SELECT CONVERT(VARCHAR, @eventCreated_at, 21)) + '",' 
			+ '"urlName":"' + @urlName + '",' 
			+ '"intervalEv":' + CAST(@intervalEv AS VARCHAR) + ',' 
			+ '"newUrl":"' + @newUrl + '",' 
			+ '"beginEv":"' + (SELECT CONVERT(VARCHAR, @beginEv, 21)) + '",' 
			+ '"endEv":"' +  (SELECT CONVERT(VARCHAR, @endEv, 21)) + '",' 
			+ '"statusEv":' + CAST(@statusEv AS VARCHAR) + ',' 
			+ '"urlsIDFK":' + CAST(@urlsIDFK AS VARCHAR) + '}]';
		SET @newStatusEv = 0;
		SET @newBeginEv = (SELECT endEv FROM #moveID WHERE moveID = @evID);
		SET @newEndEv = DATEADD(minute, @intervalEv, (SELECT endEv FROM #moveID WHERE moveID = @evID));
		SET @newPortalsSites = N'[{"eventCreated_at":"' + (SELECT CONVERT(VARCHAR, @eventCreated_at, 21)) + '",' 
			+ '"urlName":"' + @urlName + '",'
			+ '"intervalEv":' + CAST(@intervalEv AS VARCHAR) + ','
			+ '"newUrl":"' + @newUrl + '",'
			+ '"beginEv":"' + (SELECT CONVERT(VARCHAR, @newBeginEv, 21)) + '",'
			+ '"endEv":"' +  (SELECT CONVERT(VARCHAR, @newEndEv, 21)) + '",'
			+ '"statusEv":' + CAST(@newStatusEv AS VARCHAR) + ','
			+ '"urlsIDFK":' + CAST(@urlsIDFK AS VARCHAR) + '}]';
		--SELECT @portalsSitesUpdate;
		--SELECT @newPortalsSites;

		--IF (SELECT CONVERT(VARCHAR, @endEv, 21)) = @today
		--BEGIN
			--SET @notTempo = (SELECT intervalEv FROM #moveID WHERE moveID = @evID);
				--IF @notTempo = 0
			--BEGIN

			-- Le portail est actualisé avec sa dataJson et il devient actif
			-- Le portail est finalisé avec l'actualisation
			EXEC UpdatePortalsSitesWithJson @eventID, @portalsSitesUpdate;

			--SELECT @portalsSitesUpdate;
			-- Un portail est créer et il est inactif
			-- Le portail tombera dans la prochaine appel de procédure
			EXEC InsertPortalsSitesJson @newPortalsSites;
		--END
	
		DELETE #moveID WHERE moveID = @evID;
		--SELECT * FROM #moveID;
	  --SELECT * FROM dbo.PortalsSites;
	END
END