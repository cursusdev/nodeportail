USE master
GO

DROP DATABASE IF EXISTS Portals
GO

CREATE DATABASE Portals
GO

USE Portals
GO
---------------------------------------TABLE WebSites
IF OBJECT_ID('dbo.WebSites', 'U') IS NULL
CREATE TABLE dbo.WebSites (
  siteID INT PRIMARY KEY IDENTITY (1, 1),
  siteName VARCHAR(255) NOT NULL,
  siteCreated_at DATETIME DEFAULT GETDATE(),
  abName VARCHAR(50) NOT NULL,
  quotaUrls SMALLINT NOT NULL,
  quotaEv INT NOT NULL,
  duration SMALLINT NOT NULL,
  siteStatus BIT DEFAULT 0
)
-- SELECT * FROM dbo.WebSites
-- DELETE FROM dbo.WebSites
-- DROP TABLE dbo.WebSites
GO

---------------------------------------TABLE UrlsSites
IF OBJECT_ID('dbo.UrlsSites', 'U') IS NULL
CREATE TABLE dbo.UrlsSites (
  urlsID INT PRIMARY KEY IDENTITY (1, 1),
  urlsCreated_at DATETIME DEFAULT GETDATE(),
  urls NVARCHAR(MAX),
  sumUrls SMALLINT DEFAULT 0,
  sumUrlsEv BIGINT DEFAULT 0,
  siteIDFK INT
)
-- SELECT * FROM dbo.UrlsSites
-- DELETE FROM dbo.UrlsSites
-- DROP TABLE dbo.UrlsSites
GO
---------------------------------------TABLE PortalsSites
IF OBJECT_ID('dbo.PortalsSites', 'U') IS NULL
CREATE TABLE dbo.PortalsSites (
  eventID BIGINT PRIMARY KEY IDENTITY (1, 1),
  eventCreated_at DATETIME DEFAULT GETDATE(),
  urlName NVARCHAR(2083),
  intervalEv INT NULL,
  newUrl NVARCHAR(2083),
  beginEv DATETIME DEFAULT NULL,
  endEv DATETIME DEFAULT NULL,
  statusEv BIT DEFAULT 0,
  urlsIDFK INT,
  dataJson NVARCHAR(MAX)
)
-- SELECT * FROM dbo.PortalsSites
-- DELETE FROM dbo.PortalsSites
-- DROP TABLE dbo.PortalsSites
GO

ALTER TABLE dbo.UrlsSites
   ADD CONSTRAINT FK_siteID_urls FOREIGN KEY (siteIDFK)
      REFERENCES dbo.WebSites (siteID)
      ON DELETE NO ACTION
;

ALTER TABLE dbo.PortalsSites
   ADD CONSTRAINT FK_urlsID_events FOREIGN KEY (urlsIDFK)
      REFERENCES dbo.UrlsSites (urlsID)
      ON DELETE NO ACTION
;

SET IDENTITY_INSERT dbo.WebSites ON
GO

DECLARE @webSites NVARCHAR(MAX) = 
N'[{"siteID":1,"siteName":"MonSite2ouf","siteCreated_at":"22-09-2019 11:35:36.127","abName":"premium","quotaUrls":4,"quotaEv":40000,"duration":30,"siteStatus":1}]'
INSERT INTO WebSites (siteID,siteName,siteCreated_at,abName,quotaUrls,quotaEv,duration,siteStatus)
SELECT siteID,siteName,siteCreated_at,abName,quotaUrls,quotaEv,duration,siteStatus
FROM OPENJSON (@webSites) WITH(
  siteID INT,
  siteName VARCHAR(255),
  siteCreated_at DATETIME,
  abName VARCHAR(50),
  quotaUrls SMALLINT,
  quotaEv INT,
  duration SMALLINT,
  siteStatus BIT
)
GO

SET IDENTITY_INSERT dbo.WebSites OFF
GO

SET IDENTITY_INSERT dbo.UrlsSites ON
GO

DECLARE @urlsSites NVARCHAR(MAX) = 
N'[{"urlsID":1,"urlsCreated_at":"22-09-2019 11:35:36.127","urls":{"urlID":0,"url":"http:\/\/localhost\/apiJson?article=1","urlCreated_at":"22-09-2019 11:35:36.127","beginDate":"22-09-2019 11:35:36.127","endDate":null,"interval":"02:01","totEv":null,"urlStatus":1},"sumUrls":1,"sumUrlsEv":0,"siteIDFK":1}]'
INSERT INTO UrlsSites (urlsID,urlsCreated_at,urls,sumUrls,sumUrlsEv,siteIDFK)
SELECT urlsID,urlsCreated_at,urls,sumUrls,sumUrlsEv,siteIDFK
FROM OPENJSON (@urlsSites) WITH(
  urlsID INT,
  urlsCreated_at DATETIME,
  urls NVARCHAR(MAX) AS JSON,
  sumUrls SMALLINT,
  sumUrlsEv BIGINT,
  siteIDFK INT
)
GO

SET IDENTITY_INSERT dbo.UrlsSites OFF
GO

SET IDENTITY_INSERT dbo.PortalsSites ON
GO

DECLARE @portalsSites NVARCHAR(MAX) = 
N'[{"eventID":1,"eventCreated_at":"22-09-2019 11:35:36.127","urlName":"http:\/\/localhost\/apiJson?article=1","intervalEv":245,"newUrl":"?event=289&date=1567930865","beginEv":"22-09-2019 11:35:36.127","endEv":"22-09-2019 11:35:36.127","statusEv":0,"urlID":0,"urlsIDFK":1}]'
INSERT INTO PortalsSites (eventID,eventCreated_at,urlName,intervalEv,newUrl,beginEv,endEv,statusEv,urlID,urlsIDFK)
SELECT eventID,eventCreated_at,urlName,intervalEv,newUrl,beginEv,endEv,statusEv,urlID,urlsIDFK
FROM OPENJSON (@portalsSites) WITH(
  eventID BIGINT,
  eventCreated_at DATETIME,
  urlName NVARCHAR(2083),
  intervalEv INT,
  newUrl NVARCHAR(2083),
  beginEv DATETIME,
  endEv DATETIME,
  statusEv BIT,
  urlID INT,
  urlsIDFK INT
)
GO

SET IDENTITY_INSERT dbo.PortalsSites OFF
GO
