var express = require('express');
var router = express.Router();
var TYPES = require('tedious').TYPES;

/* GET UrlsSites */
router.get('/', function (req, res) {
    req.sql("SELECT urlsID,urlsCreated_at,urls,sumUrls,sumUrlsEv,siteIDFK FROM UrlsSites FOR JSON PATH, ROOT('data')")
      .into(res);
});

// // /* GET lastUrlsID */
router.get('/lastID', function (req, res) {
  // req.sql("SELECT SCOPE_IDENTITY() AS id FOR JSON PATH")
  req.sql("SELECT IDENT_CURRENT('dbo.UrlsSites') AS urlsIDmax FOR JSON PATH, WITHOUT_ARRAY_WRAPPER")
  // req.sql("SELECT @@IDENTITY AS NEW_ID FOR JSON PATH, WITHOUT_ARRAY_WRAPPER")
  // req.sql("SELECT MAX(urlsID) AS NEW_ID FROM UrlsSites FOR JSON PATH, WITHOUT_ARRAY_WRAPPER")
    .into(res);
  // req.sql("EXEC UrlsSitesLastID")
  //   .exec(res);
});


// /* GET lastUrlID */
// router.sql('/lastUrlID', function (req, res) {
//   req.sql("DECLARE @json NVARCHAR(MAX);DECLARE @urls NVARCHAR(MAX); DECLARE @req INT; SET @json = (SELECT urls FROM dbo.UrlsSites AS urls FOR JSON PATH, WITHOUT_ARRAY_WRAPPER); SET @urls = JSON_VALUE(@json, '$.urls'); PRINT @urls;")
//   .into(res);
// }

// router.get('/?siteIDFK=:id', function (req, res) {
//   req.sql("EXEC GetUrlsSitesBySite @id")
//     .param('id', req.params.id, TYPES.Int)
//     .info(res);
//   // req.sql("DECLARE @siteID INT = 1; DECLARE @json1 nvarchar(max), @json2 nvarchar(max) DECLARE @result AS nvarchar(max) SET @json1 = (SELECT IDENT_CURRENT('dbo.UrlsSites') AS urlsIDmax FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) SET @json2 = (SELECT urlsID,urlsCreated_at,urls,sumUrls,sumUrlsEv,siteIDFK FROM UrlsSites WHERE siteIDFK=@siteID FOR JSON PATH, ROOT('data')) SELECT @result = COALESCE(@result + ', ', '') + '\"' + [key] + '\":\"' + value + '\"' FROM (SELECT [key], value FROM OPENJSON(@json1) UNION ALL SELECT [key], value FROM OPENJSON(@json2)) AS x SET @result = '{' + @result + '}' PRINT @result ")
//   //   .info(res);
// });


/* GET single WebSites */
router.get('/:id', function (req, res) {
    req.sql("SELECT urlsID,urlsCreated_at,urls,sumUrls,sumUrlsEv,siteIDFK FROM UrlsSites WHERE urlsID = @id FOR JSON PATH, without_array_wrapper")
        .param('id', req.params.id, TYPES.Int)
        .into(res, '{}');
});

/* POST create UrlsSites */
router.post('/', function (req, res) {
  // res.setHeader("Content-Type", "application/json");
  // res.json({message: 'Success'});
  // fail(function(ex, res) { 
  //   res.statusCode = 500;   
  //   res.write(ex.message);
  //   res.end();
  // })
  req.sql("EXEC InsertUrlsSitesJson @json")
  .param('json', req.body, TYPES.NVarChar)
  .exec(res);
  // console.log(req.body);
  // res.end();
  // console.log(req.body);
  // const server = http.createServer((req, res) => {
  //   let data = []
  //   req.on('data', chunk => {
  //     data.push(chunk)
  //   })
  //   req.on('end', () => {
  //     JSON.parse(data).todo // 'Buy the milk'
  //   })
  // })
});



/* PUT update UrlsSites */
router.put('/:id', function (req, res) {
    req.sql("EXEC UpdateUrlsSitesJson @id, @json")
        .param('json', req.body, TYPES.NVarChar)
        .param('id', req.params.id, TYPES.Int)
        .exec(res);
});

/* DELETE delete UrlsSites */
router.delete('/:id', function (req, res) {
    req.sql("DELETE UrlsSites WHERE urlsID = @id")
        .param('id', req.params.id, TYPES.Int)
        .exec(res);
});

module.exports = router;