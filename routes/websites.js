let express = require('express');
let router = express.Router();
let TYPES = require('tedious').TYPES;

/* GET WebSites */
router.get('/', function (req, res) {
    req.sql("SELECT siteID,siteName,siteCreated_at,abName,quotaUrls,quotaEv,duration,siteStatus FROM WebSites FOR JSON PATH, ROOT('data')")
        .into(res);
});

/* GET single WebSites */
router.get('/:id', function (req, res) {
    req.sql("SELECT siteID,siteName,siteCreated_at,abName,quotaUrls,quotaEv,duration,siteStatus FROM WebSites WHERE siteID = @id FOR JSON PATH, without_array_wrapper")
        .param('id', req.params.id, TYPES.Int)
        .into(res, '{}');
});

/* POST create WebSites */
router.post('/', function (req, res) {
    req.sql("EXEC InsertWebSitesJson @json")
        .param('json', req.body, TYPES.NVarChar)
        .exec(res);
});

/* PUT update WebSites */
router.put('/:id', function (req, res) {
    req.sql("EXEC UpdateWebSitesJson @id, @json")
        .param('json', req.body, TYPES.NVarChar)
        .param('id', req.params.id, TYPES.Int)
        .exec(res);
});

/* DELETE delete WebSites */
router.delete('/:id', function (req, res) {
    req.sql("DELETE WebSites WHERE siteID = @id")
        .param('id', req.params.id, TYPES.Int)
        .exec(res);
});

module.exports = router;