let express = require('express');
let router = express.Router();
let TYPES = require('tedious').TYPES;

/* GET portail. */
router.get('/', function (req, res) {
    req.sql("SELECT eventID,eventCreated_at,urlName,intervalEv,newUrl,beginEv,endEv,statusEv,urlsIDFK,dataJson FROM PortalsSites FOR JSON PATH, ROOT('data')")
        .into(res);
});

/* GET single portail. */
router.get('/:id', function (req, res) {
    req.sql("SELECT eventID,eventCreated_at,urlName,intervalEv,newUrl,beginEv,endEv,statusEv,urlsIDFK,dataJson FROM PortalsSites WHERE eventID = @id FOR JSON PATH, without_array_wrapper")
        .param('id', req.params.id, TYPES.Int)
        .into(res, '{}');
});

/* GET single portail. */
router.get('/?urlsID=:id', function (req, res) {
  req.sql("SELECT eventID,eventCreated_at,urlName,intervalEv,newUrl,beginEv,endEv,statusEv,urlsIDFK,dataJson FROM PortalsSites WHERE urlsIDFK = @id FOR JSON PATH, without_array_wrapper")
      .param('id', req.params.id, TYPES.Int)
      .into(res, '{}');
});

/* POST create portail. */
router.post('/', function (req, res) {
  req.sql("EXEC InsertPortalsSitesJson @json")
  // req.sql("EXEC InsertPortalsSitesWithJson @json")
        .param('json', req.body, TYPES.NVarChar)
        .exec(res);
});

/* PUT update portail. */
router.put('/:id', function (req, res) {
  // req.sql("EXEC UpdatePortalsSitesJson @id, @json")
  req.sql("EXEC UpdatePortalsSitesWithJson @id, @json")
        .param('json', req.body, TYPES.NVarChar)
        .param('id', req.params.id, TYPES.Int)
        .exec(res);
});

/* DELETE delete portail. */
router.delete('/:id', function (req, res) {
    req.sql("DELETE PortalsSites WHERE eventID = @id")
        .param('id', req.params.id, TYPES.Int)
        .exec(res);
});


module.exports = router;