let express = require('express');
let router = express.Router();
let TYPES = require('tedious').TYPES;
const cron = require('node-cron');
const log = console.log;

// setup cron job
let task = cron.schedule("* * * * *", () => {
  log("logs every minute")
  // sql();
// }, {
//   scheduled: true,
//   timezone: "Europe/Paris"
});
// stops and destroys the cron job
// task.destroy();
// wont start the cron job as it has been destroyed
// task.start();
// task.stop();

// cron function
function cronSet(ms, fn) {
  function cb() {
      clearTimeout(timeout)
      timeout = setTimeout(cb, ms)
      fn()
  }
  let timeout = setTimeout(cb, ms)
  return () => {}
}
// setup middlewares
// cronSet(1000, () => log("cron job"));
// cronSet(10000, () => sql());

function sql() {

  /* GET portail. */
  router.get('/', function (req, res) {
    req.sql("SELECT dataJson FROM PortalsSites FOR JSON PATH")
    .into(res, '{}');
  });

  /* POST close and create news portails. */
  router.post('/', function (req, res) {
    req.sql("EXEC SetPortalsSites")
    .exec(res, '{}');
  });
  // console.log('Log every 10 secondes.');

};


module.exports = router;