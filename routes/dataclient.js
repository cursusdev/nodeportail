let express = require('express');
let router = express.Router();
let TYPES = require('tedious').TYPES;


/* GET websites dataJson.  http://localhost:3000/api/datas/website/event/2318 */
router.get('/website/event/:id', function (req, res) {
  req.sql("SELECT eventID,eventCreated_at,urlName,intervalEv,newUrl,beginEv,endEv,statusEv,urlsIDFK,dataJson FROM PortalsSites WHERE eventID = @id FOR JSON PATH, without_array_wrapper")
      .param('id', req.params.id, TYPES.Int)
      .into(res, '{}');
});

// router.get('/website/event=:id&date=:param', function (req, res) {
//   req.sql("SELECT eventID,eventCreated_at,urlName,intervalEv,newUrl,beginEv,endEv,statusEv,urlsIDFK,dataJson FROM PortalsSites WHERE eventID = @id AND newUrl = @param FOR JSON PATH, without_array_wrapper")
//       .param('param', req.body, TYPES.NVarChar)
//       .param('id', req.params.id, TYPES.Int)
//       .into(res, '{}');
// });


module.exports = router;