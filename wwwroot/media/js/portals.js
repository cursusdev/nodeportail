ROOT_API_URL = "/api/portals/";

// PortalsController est un objet contenant des actions sur lesquelles il sera exécuté.
// get, save (create ou update), delete
var PortalsController =
  function ($table, $modal) {
    return {

    getPortal: function (eventID) {
        $.ajax(ROOT_API_URL + eventID)
            .done( function (json) {
                $modal.loadJSON(json);
            })
            .fail(function () {
                toastr.error('Une erreur s\'est produite lors de la tentative d\'obtention du portail.');
            });
    },

    savePortal: function (eventID, portals) {
        $.ajax({
            contentType: 'application/json',
            method: (eventID == "") ? "POST" : "PUT",
            url: ROOT_API_URL + eventID,
            processData: false,
            data: portals,
        }).fail(function (msg) {
            toastr.error('Une erreur s\'est produite lors de la tentative de sauvegarde du portail.');
        }).done(function () {
            toastr.success("Portail est enregistré avec succès!");
            $table.ajax.reload(null, false);
            $modal.modal('hide');
        });
    },

    deletePortal: function (eventID) {
        $.ajax({
            method: "DELETE",
            url: ROOT_API_URL + eventID
        }).fail(function (msg) {
            toastr.error('Une erreur est survenue lors de la tentative de suppression du portail. ',' Le portail ne peut pas être supprimé!');
        }).done(function () {
            toastr.success("Le portail est supprimé avec succès!");
            $table.ajax.reload(null, false);
        });
    }
  }
};

$(document).ready(function () {

  // Configuration de DataTable
  var $table = $('#tablePortal').DataTable({
    "ajax": ROOT_API_URL,
    "columns": [
      { "data": "eventID", "defaultContent": "" },
      { "data": "eventCreated_at", "visible": true, "defaultContent": "" },
      { "data": "urlName", "visible": true, "defaultContent": "" },
      { "data": "intervalEv", "visible": true, "defaultContent": "" },
      { "data": "newUrl", "visible": true, "defaultContent": "" },
      { "data": "beginEv", "visible": true, "defaultContent": "" },
      { "data": "endEv", "visible": true, "defaultContent": "" },
      { "data": "statusEv", "visible": true, "defaultContent": "" },
      { "data": "siteIDFK", "visible": true, "defaultContent": "" },
      { "data": "siteIDFK", "visible": true, "defaultContent": "" },
      {
        "data": "eventID",
        "sortable": false,
        "render": function (data) {
          return '<button data-id="' + data + '" class="btn btn-primary btn-sm edit" data-toggle="modal" data-target="#portalModal"><span class="glyphicon glyphicon-edit"></span> Edit</button>';
        }
      },
      {
        "data": "eventID",
        "sortable": false,
        "render": function (data) {
          return '<button data-id="' + data + '" class="btn btn-danger btn-sm delete"><span class="glyphicon glyphicon-remove"></span> Delete</button>';
        }
      }
    ]
  });// fin de la configuration de DataTable
  
  // configuration du modal Bootstrap
  $modal = $('#portalModal');

  $modal.on('hide.bs.modal', function () {
    $(this).find("input[type!=checkbox],textarea,select").val('').end();
    $(this).find("input:checkbox").prop('checked', false);
  });

  $("#cancelPortal", $modal).on("click", function () {
    $modal.modal('hide');
  });
  // fin de la configuration modal

  var ctrl = PortalsController($table, $modal);

  $table.on("click", "button.edit",
  function () {
    ctrl.getPortal(this.attributes["data-id"].value);
  });

  $table.on("click", "button.delete",
  function () {
    ctrl.deletePortal(this.attributes["data-id"].value);
  });

  $('body').on("click", "#submitPortal",
  function (e) {
    e.preventDefault();
    var $form = $("#portalForm");
    var eventId = $("#eventID", $form).val();
    var portal = JSON.stringify($form.serializeJSON({ checkboxUncheckedValue: "false", parseAll: true }));
    ctrl.savePortal(eventId, portal);
  });
});