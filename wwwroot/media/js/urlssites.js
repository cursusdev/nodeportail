ROOT_API_URL = "/api/urlssites/";

// UrlssitesController est un objet contenant des actions sur lesquelles il sera exécuté.
// get, save (create ou update), delete
var UrlssitesController =
  function ($table, $modal) {
    return {

    getWebsite: function (urlsID) {
        $.ajax(ROOT_API_URL + urlsID)
            .done( function (json) {
                $modal.loadJSON(json);
            })
            .fail(function () {
                toastr.error('Une erreur s\'est produite lors de la tentative d\'obtention du website.');
            });
    },

    saveWebsite: function (urlsID, urlssites) {
        $.ajax({
            contentType: 'application/json',
            method: (urlsID == "") ? "POST" : "PUT",
            url: ROOT_API_URL + urlsID,
            processData: false,
            data: urlssites,
        }).fail(function (msg) {
            toastr.error('Une erreur s\'est produite lors de la tentative de sauvegarde du website.');
        }).done(function () {
            toastr.success("Website est enregistré avec succès!");
            $table.ajax.reload(null, false);
            $modal.modal('hide');
        });
    },

    deleteWebsite: function (urlsID) {
        $.ajax({
            method: "DELETE",
            url: ROOT_API_URL + urlsID
        }).fail(function (msg) {
            toastr.error('Une erreur est survenue lors de la tentative de suppression du website. ',' Le website ne peut pas être supprimé!');
        }).done(function () {
            toastr.success("Le website est supprimé avec succès!");
            $table.ajax.reload(null, false);
        });
    }
  }
};

$(document).ready(function () {

  // Configuration de DataTable
  var $table = $('#tableUrlssite').DataTable({
    "ajax": ROOT_API_URL,
    "columns": [
      { "data": "urlsID", "defaultContent": "" },
      { "data": "urlsCreated_at", "visible": true, "defaultContent": "" },
      { "data": "urls", "visible": true, "defaultContent": "" },
      { "data": "sumUrls", "visible": true, "defaultContent": "" },
      { "data": "sumUrlsEv", "visible": true, "defaultContent": "" },
      { "data": "siteIDFK", "visible": true, "defaultContent": "" },
      {
        "data": "urlsID",
        "sortable": false,
        "render": function (data) {
          return '<button data-id="' + data + '" class="btn btn-primary btn-sm edit" data-toggle="modal" data-target="#urlssiteModal"><span class="glyphicon glyphicon-edit"></span> Edit</button>';
        }
      },
      {
        "data": "urlsID",
        "sortable": false,
        "render": function (data) {
          return '<button data-id="' + data + '" class="btn btn-danger btn-sm delete"><span class="glyphicon glyphicon-remove"></span> Delete</button>';
        }
      }
    ]
  });// fin de la configuration de DataTable
  
  // configuration du modal Bootstrap
  $modal = $('#urlssiteModal');

  $modal.on('hide.bs.modal', function () {
    $(this).find("input[type!=checkbox],textarea,select").val('').end();
    $(this).find("input:checkbox").prop('checked', false);
  });

  $("#cancelUrlssite", $modal).on("click", function () {
    $modal.modal('hide');
  });
  // fin de la configuration modal

  var ctrl = UrlssitesController($table, $modal);

  $table.on("click", "button.edit",
  function () {
    ctrl.getWebsite(this.attributes["data-id"].value);
  });

  $table.on("click", "button.delete",
  function () {
    ctrl.deleteWebsite(this.attributes["data-id"].value);
  });

  $('body').on("click", "#submitUrlssite",
  function (e) {
    e.preventDefault();
    var $form = $("#urlssiteForm");
    var urlsID = $("#urlsID", $form).val();
    var urlssite = JSON.stringify($form.serializeJSON({ checkboxUncheckedValue: "false", parseAll: true }));
    ctrl.saveWebsite(urlsID, urlssite);
  });
});