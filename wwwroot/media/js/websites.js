ROOT_API_URL = "/api/websites/";

// WebsitesController est un objet contenant des actions sur lesquelles il sera exécuté.
// get, save (create ou update), delete
var WebsitesController =
  function ($table, $modal) {
    return {

    getWebsite: function (siteID) {
        $.ajax(ROOT_API_URL + siteID)
            .done( function (json) {
                $modal.loadJSON(json);
            })
            .fail(function () {
                toastr.error('Une erreur s\'est produite lors de la tentative d\'obtention du website.');
            });
    },

    saveWebsite: function (siteID, websites) {
        $.ajax({
            contentType: 'application/json',
            method: (siteID == "") ? "POST" : "PUT",
            url: ROOT_API_URL + siteID,
            processData: false,
            data: websites,
        }).fail(function (msg) {
            toastr.error('Une erreur s\'est produite lors de la tentative de sauvegarde du website.');
        }).done(function () {
            toastr.success("Website est enregistré avec succès!");
            $table.ajax.reload(null, false);
            $modal.modal('hide');
        });
    },

    deleteWebsite: function (siteID) {
        $.ajax({
            method: "DELETE",
            url: ROOT_API_URL + siteID
        }).fail(function (msg) {
            toastr.error('Une erreur est survenue lors de la tentative de suppression du website. ',' Le website ne peut pas être supprimé!');
        }).done(function () {
            toastr.success("Le website est supprimé avec succès!");
            $table.ajax.reload(null, false);
        });
    }
  }
};

$(document).ready(function () {

  // Configuration de DataTable
  var $table = $('#tableWebsite').DataTable({
    "ajax": ROOT_API_URL,
    "columns": [
      { "data": "siteID", "defaultContent": "" },
      { "data": "siteName", "visible": true, "defaultContent": "" },
      { "data": "siteCreated_at", "visible": true, "defaultContent": "" },
      { "data": "abName", "visible": true, "defaultContent": "" },
      { "data": "quotaUrls", "visible": true, "defaultContent": "" },
      { "data": "quotaEv", "visible": true, "defaultContent": "" },
      { "data": "duration", "visible": true, "defaultContent": "" },
      { "data": "siteStatus", "visible": true, "defaultContent": "" },
      {
        "data": "siteID",
        "sortable": false,
        "render": function (data) {
          return '<button data-id="' + data + '" class="btn btn-primary btn-sm edit" data-toggle="modal" data-target="#websiteModal"><span class="glyphicon glyphicon-edit"></span> Edit</button>';
        }
      },
      {
        "data": "siteID",
        "sortable": false,
        "render": function (data) {
          return '<button data-id="' + data + '" class="btn btn-danger btn-sm delete"><span class="glyphicon glyphicon-remove"></span> Delete</button>';
        }
      }
    ]
  });// fin de la configuration de DataTable
  
  // configuration du modal Bootstrap
  $modal = $('#websiteModal');

  $modal.on('hide.bs.modal', function () {
    $(this).find("input[type!=checkbox],textarea,select").val('').end();
    $(this).find("input:checkbox").prop('checked', false);
  });

  $("#cancelWebsite", $modal).on("click", function () {
    $modal.modal('hide');
  });
  // fin de la configuration modal

  var ctrl = WebsitesController($table, $modal);

  $table.on("click", "button.edit",
  function () {
    ctrl.getWebsite(this.attributes["data-id"].value);
  });

  $table.on("click", "button.delete",
  function () {
    ctrl.deleteWebsite(this.attributes["data-id"].value);
  });

  $('body').on("click", "#submitWebsite",
  function (e) {
    e.preventDefault();
    var $form = $("#websiteForm");
    var siteID = $("#siteID", $form).val();
    var website = JSON.stringify($form.serializeJSON({ checkboxUncheckedValue: "false", parseAll: true }));
    ctrl.saveWebsite(siteID, website);
  });
});